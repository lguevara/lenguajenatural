import codecs
from os import path
import logging

class Extractor:
    def __init__(self, ruta):
        self.ruta = ruta

    def extraer_palabras(self):
        f = codecs.open(self.ruta, "r", "utf-8")
        data = f.read()
        s = set(data.split())

        return list(s)


if __name__ == "__main__":
    ruta = path.join("ejemplo","es.dict")
    nombre_archivo = path.basename(ruta)

    salida = open(path.join("data",nombre_archivo+".palabras"),"w")

    extractor = Extractor(ruta)
    lista = extractor.extraer_palabras()

    for elemento in lista:
        try:
            salida.write(elemento)
            salida.write("\n")
        except:
            pass